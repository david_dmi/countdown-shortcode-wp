<?php   
    /*
    Import styles from parent theme and enque child theme styles
    * @author - dportilla
    * @since 29-04-2018
    * @param string parent_style: twentyfifteen
    * @return mixed styles from parant and child theme 
    */
    function add_theme_scripts() {
        // Load parent styles.
     $parent_style = 'twentyfifteen';
     wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );   
        // Load child styles.
     wp_enqueue_style('mainCSS',
         get_stylesheet_directory_uri() . '/css/main.min.css',
         array( $parent_style ),
         wp_get_theme()->get('Version')
     );
       // Load scripts
     wp_enqueue_script( 'countdown', get_stylesheet_directory_uri(). '/js/countdown.js', null, null, true );	
       // Localize registered script to use in countdown.js
       // wp_localize_script('countdown','fecha',$my_array_variable);
 }
 add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );

    /*
    Create Shorcode with two parameters
    * @author - dportilla
    * @since 29-04-2018
    * @param 'title' 'remaining'
    * @return and html for the counter
    */
    
    // shortcode CTA
    function my_shortcode( $atts ) { 
        $input = shortcode_atts( array(
            'title' => 'Our NFL Pick: Coming Soon',
            'remaining' => '0'
        ), $atts );    

        return     
        '<section id="banner-bet">
        <img src=" '. get_stylesheet_directory_uri() . '/img/football-player.png'    .'" alt="player">
        
        <div class="info-handler">

        <div id="countdown"> 

            <span class="day">
                <p class="text">DAYS</p>
                <p id="dayNumber" class="number">00</p>
            </span>
            <span class="hours">
                <p class="text">HOURS</p>
                <p id="hourNumber" class="number">00</p>
            </span>
            <span class="minutes">
                <p class="text">MIN</p>
                <p id="minuteNumber" class="number">00</p>
            </span>
            <span class="seconds">
                <p class="text">SEC</p>
                <p id="secondNumber" class="number">00</p>
            </span>

        </div>
        <div id="timeData" style="display: none;"> '.  $input['remaining'] .' </div>
        <!-- #countdown -->

        <p id="timeEnd" class="remain"> Remaining Time To Place Bet </p>            
        <h1 class="suggest">'.  $input['title'] .'</h1>
        <p class="hurry"> Hurry up! <span> 25 </span> people have placed this bet </p>

        </div>
        <!-- .info-handler -->

        <div class="bet-handler">
        <button> Bet & Win </button>
        <p class="trusted">Trusted <br> Sportsbetting.ag</p>
        </div>       
        </section>
        ';     
    }
    add_shortcode( 'cta', 'my_shortcode' );
?>