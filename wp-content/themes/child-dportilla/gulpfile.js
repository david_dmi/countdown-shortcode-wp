const
  gulp = require("gulp"),                         // require gulp
  sass = require("gulp-sass"),                    // Gulp plugin for Sass which is based on libSass
  autoprefixer = require("gulp-autoprefixer"),    // plugin to auto prefix CSS for legacy browsers
  rename = require("gulp-rename"),                // plugin to easily rename files
  uglifycss = require("gulp-uglifycss"),          // Minifies CSS files
  plumber = require("gulp-plumber"),              // Plumber — Fix node pipes, prevent them from breaking due to an error
  notify = require("gulp-notify"),                // Sends message notification to me
  browserSync = require("browser-sync").create(); // Live reload
reload = browserSync.reload;


//-----------------
// on error show message
//-----------------

let plumberErrorHandler = {
  errorHandler: notify.onError({
    title: "Gulp",
    message: "Error: <%= error.message %>"
  })
};

//-----------------
// SASS task
//-----------------
gulp.task("sass", () => {
  return gulp
    .src("./sass/*.scss")
    // .pipe(plumber(plumberErrorHandler)) // cacth errors
    .pipe(
      sass({
        outputStyle: "compressed",
        // 'compact' / 'nested' / 'expanded'
        errLogToConsole: true,
        sourceComments: false
      }).on("error", sass.logError)
    )
    // add CSS vendors 
    .pipe(
      autoprefixer({
        browsers: [
          "last 2 version",
          "> 1%",
          "safari 5",
          "ie 8",
          "ie 9",
          "opera 12.1",
          "ios 6",
          "android 4"
        ],
        cascade: false
      })
    )
    .pipe(plumber.stop())
    .pipe(gulp.dest("./css"))
});

//-----------------
// CSS task
//-----------------
gulp.task("uglify-css", () => {
  return gulp
    .src("./css/main.css")
    .pipe(uglifycss({ maxLineLen: 10, uglyComments: true, debug: true }))
    // .pipe(uglifycss( /* {maxLineLen:10} */ ))
    .pipe(rename({ extname: ".min.css" }))
    .pipe(gulp.dest("./css"))
    // .pipe(browserSync.stream()) // Inject Styles when style file is created
    .pipe(reload({ stream: true }))   // Inject Styles when style file is created
  // .pipe(notify({ message: 'TASK: "uglify-css" Completed!', onLast: true }))
});

//-----------------
// local server task
//-----------------
gulp.task('browser-sync', function () {
  browserSync.init({
    // server: { baseDir: './' },
    proxy: "localhost/ctas/",
    notify: true,
    // port: 8080,
    // Inject CSS changes
    injectChanges: true
  });
})

//-----------------
// Default task to be run with `gulp`
//-----------------
gulp.task('default', ['browser-sync'], () => {

  gulp.watch('./sass/**/*.scss', ["sass"])            // look for changes on SASS
  gulp.watch('./css/**/*.css', ["uglify-css"])        // look for changes on CSS

  // gulp.watch('./**/*.php').on('change', browserSync.reload)    // look for changes on PHP
  gulp.watch('./*.php').on('change', browserSync.reload)          // look for changes on PHP

  // gulp.watch('./**/*.js').on('change', browserSync.reload)     // look for changes on JS
  gulp.watch('./js/*.js').on('change', browserSync.reload)        // look for changes on JS

  gulp.watch('./*.{png,jpg,gif,svg}').on('change', browserSync.reload)      // look for changes on IMG 
  gulp.watch('./img/*.{png,jpg,gif,svg}').on('change', browserSync.reload); // look for changes on IMG 
});