// get the Date
var desiredDate = document.getElementById('timeData');
console.log('Date: '+desiredDate.innerHTML);

// var countDownDate = new Date("May 10, 2018 20:42:30").getTime();
var countDownDate = new Date(desiredDate.innerHTML).getTime();
 
// shorcode options available
// [cta title]
// [cta title="Our NFL Pick: Patriots +5"]
// [cta remaining="2018/04/29 19:45:00"]
// [cta title="Our NFL Pick: Patriots +5" remaining="2018/04/29 19:45:00"]

function timer() {
    // Get todays date and time
    var now = new Date().getTime();
   
    // Find the distance between now an the count down date
    var distance = countDownDate - now;

    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    // If the count down is over, write some text 
    if (distance < 0) {
        clearInterval(timer);         
        document.getElementById("timeEnd").innerHTML = "Time Expired";
    }else{
        window.onload = setInterval(timer, 1000);  
        // Output the result in an element with ID
        document.getElementById("dayNumber").innerHTML = days;
        document.getElementById("hourNumber").innerHTML = hours;
        document.getElementById("minuteNumber").innerHTML = minutes;
        document.getElementById("secondNumber").innerHTML = seconds;    
    }
}

window.onload = setTimeout(timer, 0);